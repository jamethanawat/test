﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace stockproject.Models
{
    public class AdminConfirm
    {
        [Key]
        public string product { get; set; }
        public int? qty { get; set; }
        public float? price { get; set; }
        public DateTime? Createdate { get; set; }
        public string status { get; set; }
        public string name { get; set; }
    }
}
