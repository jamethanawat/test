﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace stockproject.Models
{
    public class QueryAdmin
    {
        [Key]
        public int id { get; set; }
        public string Subject{ get; set; }
        public DateTime? Createdate { get; set; }
        public string Status { get; set; }

  
    }
}
