﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using stockproject.Enititys;
using stockproject.Models;

namespace stockproject.Pages
{
    public class IndexModel : PageModel
    {
        private readonly StockprojectContext db = new StockprojectContext();
        public void OnGet()
        {

        }
        public ActionResult OnPostLogInUser()
        {
             
            Enititys.User user = new Enititys.User();
            user = (from t1 in db.User
                    where t1.Username == username && t1.Password == password
                    select t1).SingleOrDefault();

            if (user != null)
            {

                var claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.Name, user.Username));

                var claimIdenties = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimPrincipal = new ClaimsPrincipal(claimIdenties);
                var authenticationManager = Request.HttpContext;

                authenticationManager.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimPrincipal, new AuthenticationProperties() { IsPersistent = false });

                //return RedirectToPage("/Stock/Admin/Admin");

                return RedirectToPage("/Stock/Product");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid username or password.");
                return Page();
            }

        }
        //public List<User> MyProperty = new List<User>();
        [BindProperty]
        public string username { get; set; }
        [BindProperty]
        public string password { get; set; }
    }
}

