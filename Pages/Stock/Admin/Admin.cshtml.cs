﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using stockproject.Enititys;
using stockproject.Models;

namespace stockproject.Pages.Admin
{
    public class AdminModel : PageModel
    {
        private  readonly StockprojectContext db = new StockprojectContext();

        
        public void OnGet()
        {
            //var q2 = from a in db.User
            //         join e in db.FormIn on a.Userid equals e.Userid

            //         select new { a.Userid,a.Username,e.Createdate, e.Status };
            var q2 = (from t1 in db.FormIn
                      orderby t1.Status ascending
                      select new { t1.FormInid , t1.Status, t1.Createdate});
                      
                     
            foreach (var item in q2)
            {
                var status = "";
                if (item.Status ==true)
                {
                    status = "True";
                }else
                {
                    status = "False";
                }

                list.Add(new QueryAdmin
                {

                    id = item.FormInid,
                   Subject = "FormIn",
                    Createdate = item.Createdate,
                    Status = status

                });
            }
            var q3= (from t1 in db.FormOut
                      orderby t1.Status ascending
                      select new { t1.FormOutid, t1.Status, t1.Createdate });


            foreach (var item in q3)
            {
                var status = "";
                if (item.Status == true)
                {
                    status = "True";
                }
                else
                {
                    status = "False";
                }

                list.Add(new QueryAdmin
                {

                    id = item.FormOutid,
                    Subject = "FormOut",
                    Createdate = item.Createdate,
                    Status = status

                });
            }

        }
        
        public ActionResult OnPostAAA()
        {
            //var a = MyProperty1;
            return Page();
        }
        //public IActionResult OnPost()
        //{
        //    return Content("");
        //}
        public PartialViewResult OnGetContactModalPartial()
        {
            // this handler returns _ContactModalPartial
            return new PartialViewResult
            {
                //ViewName = "_ContactModalPartial",
                //ViewData = new ViewDataDictionary<Contact>(ViewData, new Contact { })
            };
        }
        public ActionResult OnPostVALUE(int id,string type)
        {
         
            return RedirectToPage("./confirmstatus", "OnGet", new { formid = id ,formtype=type });

        }
    
        [BindProperty]
        public List<QueryAdmin> list { get; set; } = new List<QueryAdmin>();
        public List<string> id { get; set; }

    }
}