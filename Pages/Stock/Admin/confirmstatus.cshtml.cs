﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;
using stockproject.Models;

namespace stockproject
{
    public class confirmstatusModel : PageModel
    {
 

        private StockprojectContext db = new StockprojectContext();
        public List<AdminConfirm> list { get; set; } = new List<AdminConfirm>();
        public void OnGet(int formid , string formtype)
        {
            if (formtype == "FormIn")
            {
           
                ViewData["id"] = formid;
                ViewData["type"] = "Form IN";
                var q2 = from a in db.ProductFormIn
                         join b in db.FormIn on a.FormInid equals b.FormInid
                         join c in db.User on b.Userid equals c.Userid
                         join d in db.Product on a.ProductId equals d.Productid
                         where (b.FormInid == formid)
                         select new { c.Username, b.Createdate, d.Productname, a.Qty, a.Price };
                foreach (var item in q2)
                {

                    ViewData["date"] = item.Createdate;
                    ViewData["name"] = item.Username;
                    list.Add(new AdminConfirm
                    {
                        product = item.Productname,
                        qty = item.Qty,
                       // Createdate = item.Createdate,
                        price = item.Price
                        //name= item.Username
                    });
                }
            }
            else
            {
                ViewData["id"] = formid;
                ViewData["type"] = "Form OUT";
                var q2 = from a in db.ProductFormOut
                     join b in db.FormOut on a.FormOutid equals b.FormOutid
                     join c in db.User on b.Userid equals c.Userid
                     join d in db.Product on a.ProductId equals d.Productid
                         where (b.FormOutid == formid)
                         select new { c.Username, b.Createdate ,d.Productname,a.Qty, a.Price };

                foreach (var item in q2)
                {

                    ViewData["date"] = item.Createdate;
                    ViewData["name"] = item.Username;
                    list.Add(new AdminConfirm
                    {
                        product = item.Productname,
                        qty = item.Qty,
                        // Createdate = item.Createdate,
                        price = item.Price
                        //name= item.Username
                    });
                }

            }
        }

        public ActionResult OnPostCONFIRM(string id,string type)
        {
            if (type == "Form IN")
            {
               
                var q = from c in db.FormIn
                         where c.FormInid == Int32.Parse(id)
                         select c;
                foreach (FormIn c in q)
                {
                    c.Status = true;
                }
                db.SaveChanges();

            }
            else if(type == "Form OUT")
            {
                var q = from d in db.FormOut
                        where d.FormOutid == Int32.Parse(id)
                        select d;
                foreach (FormOut d in q)
                {
                    d.Status = true;
                }
                db.SaveChanges();
            }
            return RedirectToPage("./Admin", "OnGet");

        }
        public ActionResult OnPostREJECT(string id, string type)
        {

            if (type == "Form IN")
            {

            }
            else if (type == "Form OUT")
            {

            }
            return RedirectToPage("./Admin", "OnGet");

        }
    }
}