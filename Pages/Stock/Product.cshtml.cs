﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;
using stockproject.Models;

namespace stockproject.Pages.Stock
{
    public class ProductModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();
        public List<Product> list { get; set; } = new List<Product>();
        public void OnGet()
        {
            var q = (from t1 in db.Product
                      orderby t1.Productname ascending
                      select new { t1.Productname, t1.Qty, t1.Price});


            foreach (var item in q)
            {

                list.Add(new Product
                {

                    Productname = item.Productname,
                    Qty = item.Qty,
                    Price = item.Price,

                });
            }
        }
    }
}