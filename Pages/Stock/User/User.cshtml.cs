﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using stockproject.Models;

namespace stockproject.Pages.User
{
    public class UsersModel : PageModel
    {
        public ActionResult OnGet(string A1)
        {
            MyProperty = new List<Class> {
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"},
            new Class { A1 = "2" , A2 = "1"} 
            };

            return Page();
        }
        public ActionResult OnPostAAA()
        {
            var a = MyProperty1;
           return Page();
        }
        [BindProperty]
        public int MyProperty1 { get; set; }

        [BindProperty]
        public List<Class> MyProperty { get; set; } = new List<Class>();

    }
}