﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using stockproject.Enititys;

namespace stockproject
{
    public class CreateProductModel : PageModel
    {
        private StockprojectContext db = new StockprojectContext();
        public void OnGet()
        {
        }
        public ActionResult OnPostToCreate()
        {
            var a = name;
            var a1 = price;
            Product table = new Product();
            table.Productname = name;
            table.Qty = 1;
            table.Price = Int32.Parse(price);
            table.Userid = 1;
            table.Modifydate = DateTime.Now;

            db.Product.Add(table);
            db.SaveChanges();
            return RedirectToPage("./Product", "OnGet");
        }
        [BindProperty]
        public string name { get; set; }
        [BindProperty]
        public string price { get; set; }
    }
}